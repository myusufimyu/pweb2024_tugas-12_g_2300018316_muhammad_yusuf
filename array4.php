<?php
$arrNilai = array("Fulan" => 80, "Fulin" => 90, "Fulun" => 75, "falan" => 85);

echo "Menampilkan isi array asosiatif dengan foreach: <br>";
foreach ($arrNilai as $nama => $nilai) {
    echo "Nilai $nama = $nilai<br>";
}

echo "<br>Menampilkan isi array asosiatif dengan WHILE:<br>";
$keys = array_keys($arrNilai); 
$index = 0; 
while ($index < count($arrNilai)) { 
    $nama = $keys[$index]; 
    $nilai = $arrNilai[$nama]; 
    echo "Nilai $nama = $nilai<br>";
    $index++; 
}
?>
