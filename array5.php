<?php

$warna = ["Blue", "Black", "Red", "Yellow", "Green"];
$nilai = [
    "Fulan" => 80,
    "Fulin" => 90,
    "fulun" => 75,
    "Falan" => 85
];

echo "<pre>";

echo "Array Warna:\n";
var_dump($warna);

echo "\nArray Nilai:\n";
foreach ($nilai as $nama => $nilai) {
    echo "$nama: $nilai\n";
}

echo "</pre>";
?>
