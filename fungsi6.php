<?php

function luas_Lingkaran($jari) {
    return 3.14 * $jari * $jari;
}

$namaFungsi = "luas_Lingkaran"; 

if (function_exists($namaFungsi)) {
    echo "Fungsi '$namaFungsi' ada pada versi PHP ini.\n";
} else {
    echo "Fungsi '$namaFungsi' tidak ditemukan pada versi PHP ini.\n";
}
