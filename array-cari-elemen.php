<?php

$daftarBuah = ["Mangga", "Apel", "Pisang", "Kedondong", "Jeruk"];

$buahDicari = "Kedondong";

if (array_search($buahDicari, $daftarBuah) !== false) { 
    echo "Ada buah $buahDicari di dalam array tersebut!";
} else {
    echo "Tidak ada buah $buahDicari di dalam array tersebut.";
}
?>
